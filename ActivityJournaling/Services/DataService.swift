//
//  BettermentsAndStruggleService.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/28/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class DataService {
    func getEntries(date: String?, type: Int?, _ callback: @escaping ([DataDto]?, AJError?) -> Void) -> Void {
        let request = GetEntriesRequest()
        request.type = type
        request.date = date
        
        SessionManager.dataTask(url: ServiceURLs.Data.getEntriesUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(nil, errorParam)
                return
            }else if let errorParam = $1 {
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let data = $0 as? [Any]
            var entries = [DataDto]()
            
            for restEntry in data!{
                let entry = DataDto()
                do {
                    try entry.load(data: restEntry)
                }
                catch{
                    callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
                    return
                }
                
                entries.append(entry)
            }
            
            callback(entries, nil)
        }
    }
    
    func getEntry(id: Int?, type: Int?, _ callback: @escaping (DataDto?, AJError?) -> Void) -> Void{
        let request = GetOrDeleteEntryRequest()
        request.id = id
        request.type = type
        
        SessionManager.dataTask(url: ServiceURLs.Data.getEntryUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(nil, errorParam)
                return
            }else if let errorParam = $1 {
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let entry = DataDto()
            do {
                try entry.load(data: $0 as Any)
            } catch {
                callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
            }
            
            callback(entry, nil)
        }
    }
    
    func saveEntry(id: Int?, subject: String?, entry: String?, date: String?, tags: String?, type: Int?, _ callback: @escaping (AJError?) -> Void) -> Void{
        let request = SaveEntryRequest()
        request.date = date
        request.entry = entry
        request.subject = subject
        request.id = id
        request.tags = tags
        request.type = type
        
        SessionManager.dataTask(url: ServiceURLs.Data.saveEntryUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(errorParam)
                return
            }else if let errorParam = $1 {
                callback(AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            callback(nil)
        }
    }
    
    func deleteEntry(id: Int?, type: Int?, _ callback: @escaping (AJError?) -> Void) -> Void{
        let request = GetOrDeleteEntryRequest()
        request.id = id
        request.type = type
        
        SessionManager.dataTask(url: ServiceURLs.Data.deleteEntryUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(errorParam)
                return
            }else if let errorParam = $1 {
                callback(AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            callback(nil)
        }
    }
    
    func getDataStatus(_ callback: @escaping (DataStatusDto?, AJError?) -> Void) -> Void{
        
        SessionManager.dataTask(url: ServiceURLs.Data.getDataStatus, data: nil){
            if let errorParam = $1 as? AJError{
                callback(nil, errorParam)
                return
            }else if let errorParam = $1 {
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let entry = DataStatusDto()
            do {
                try entry.load(data: $0 as Any)
            } catch {
                callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
            }
            
            callback(entry, nil)
        }
    }
}
