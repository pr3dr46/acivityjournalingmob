//
//  TaskService.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/20/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation
import EventKit

class TaskService {
    func getTasks(date: String?, status: Int?, _ callback: @escaping ([TaskDto]?, AJError?) -> Void) -> Void {
        let request = GetTasksRequest()
        request.status = status
        request.date = date
        
        SessionManager.dataTask(url: ServiceURLs.Task.getTasksUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(nil, errorParam)
                return
            }else if let errorParam = $1 {
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let data = $0 as? [Any]
            var tasks = [TaskDto]()
            
            for restTask in data!{
                let task = TaskDto()
                do {
                    try task.load(data: restTask)
                }
                catch{
                    callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
                    return
                }
               
                tasks.append(task)
            }
            
            callback(tasks, nil)
        }
    }
    
    func getTask(id: Int?, _ callback: @escaping (TaskDto?, AJError?) -> Void) -> Void{
        let request = GetOrDeleteTaskRequest(id: id)
        
        SessionManager.dataTask(url: ServiceURLs.Task.getTaskUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(nil, errorParam)
                return
            }else if let errorParam = $1 {
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let task = TaskDto()
            do {
                try task.load(data: $0 as Any)
            } catch {
                callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
            }
            
            callback(task, nil)
        }
    }
    
    func saveTask(id: Int?, subject: String?, description: String?, date: String?, tags: String?, comment: String?, emojiState: Int?, eventIdentifier: String?, _ callback: @escaping (TaskSavedDto?, AJError?) -> Void) -> Void{
        let request = SaveTaskRequest()
        request.date = date
        request.description = description
        request.subject = subject
        request.id = id
        request.tags = tags
        request.comment = comment
        request.emojiState = emojiState
        request.calendarEventIdentifier = eventIdentifier
        
        SessionManager.dataTask(url: ServiceURLs.Task.saveTaskUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(nil, errorParam)
                return
            }else if let errorParam = $1 {
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let model = TaskSavedDto()
            do {
                try model.load(data: $0 as Any)
            } catch {
                callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
            }
            
            callback(model, nil)
        }
    }
    
    func deleteTask(id: Int?, _ callback: @escaping (AJError?) -> Void) -> Void{
        let request = GetOrDeleteTaskRequest(id: id)
        
        SessionManager.dataTask(url: ServiceURLs.Task.deleteTaskUrl, data: request){
            if let errorParam = $1 as? AJError{
                callback(errorParam)
                return
            }else if let errorParam = $1 {
                callback(AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            callback(nil)
        }
    }
    
    func updateTaskStatus(id: Int?, status: Int?, _ callback: @escaping (AJError?) -> Void) -> Void{
        let request = UpdateTaskStatusRequest()
        request.id = id
        request.status = status
        
        SessionManager.dataTask(url: ServiceURLs.Task.updateStatusUrl, data: request){
            if let errorParam = $1{
                callback(AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            callback(nil)
        }
    }
    
    func getTasksStatus(_ callback: @escaping (TasksStatusDto?, AJError?) -> Void) -> Void{
        
        SessionManager.dataTask(url: ServiceURLs.Task.getTasksStatus, data: nil){
            if let errorParam = $1 as? AJError{
                callback(nil, errorParam)
                return
            }else if let errorParam = $1 {
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let model = TasksStatusDto()
            do {
                try model.load(data: $0 as Any)
            } catch {
                callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
            }
            
            callback(model, nil)
        }
    }
    
    func addEventToCalendar(identifier: String?, title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ identifier: String?, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event: EKEvent
                
                if identifier != nil{
                    let eventOption = eventStore.event(withIdentifier: identifier!)
                    if eventOption != nil{
                        event = eventOption!
                    } else{
                        return
                    }
                }else{
                    event = EKEvent(eventStore: eventStore)
                }
          
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(nil, e)
                    return
                }
                completion?(event.eventIdentifier, nil)
            } else {
                completion?(nil, error as NSError?)
            }
        })
    }
    
    func removeEventFromCalendar(identifier: String, completion: ((_ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event: EKEvent
                
                let eventOption = eventStore.event(withIdentifier: identifier)
                if eventOption != nil{
                    event = eventOption!
                } else{
                    completion?(nil)
                    return
                }
                
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.remove(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(e)
                    return
                }
                completion?(nil)
            } else {
                completion?(error as NSError?)
            }
        })
    }
    
    func changeEventCalendarStatus(identifier: String, completed: Bool, completion: ((_ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event: EKEvent
                
                let eventOption = eventStore.event(withIdentifier: identifier)
                if eventOption != nil{
                    event = eventOption!
                } else{
                    completion?(nil)
                    return
                }
                
                event.calendar = eventStore.defaultCalendarForNewEvents
                //todo
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(e)
                    return
                }
                completion?(nil)
            } else {
                completion?(error as NSError?)
            }
        })
    }
}
