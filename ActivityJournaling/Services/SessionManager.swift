//
//  SessionManager.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/24/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class SessionManager{

    static func dataTask(url: String, data: ToUrlParametersProtocol?, completionHandler: @escaping (Any?, Error?) -> Swift.Void){
        var todosEndpoint = url
        if data != nil {
            todosEndpoint = todosEndpoint + "?" + data!.toParameters()
        }
        
        guard let todosURL = URL(string: todosEndpoint) else {
            let error = AJError(description: "Cannot create URL", kind: .SessionError)
            AJLogger.logError(error)
            completionHandler(nil, error)
            return
        }
        var todosUrlRequest = URLRequest(url: todosURL)
        todosUrlRequest.httpMethod = "POST"

        todosUrlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        todosUrlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let sessionConfig = URLSessionConfiguration.default
        let authValue: String? = "Bearer \((AuthenticationService.getToken())!)"
        sessionConfig.httpAdditionalHeaders = ["Authorization": authValue ?? ""]
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        let task = session.dataTask(with: todosUrlRequest) {
            (data, response, error) in
            guard error == nil else {
                AJLogger.logError(error)
                completionHandler(nil, AJError(description: error!.localizedDescription, kind: .SessionError))
                return
            }
            guard let responseData = data else {
                AJLogger.logError("Error: did not receive data")
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let responseRaw = try JSONSerialization.jsonObject(with: responseData,
                                                                         options: []) as? [String: Any] else {
                                                                            let error = AJError(description: "Could not get JSON from responseData as dictionary", kind: .SessionError)
                                                                            AJLogger.logError(error)
                                                                            completionHandler(nil, error)
                                                                            return
                }
                
                let response = JsonResponse()
                response.load(dict: responseRaw)
                
                if response.status == ResponseStatuses.error{
                    let error = AJError(description: response.message, kind: .SessionError)
                    AJLogger.logError(error)
                    completionHandler(nil, error)
                    return
                }
                
                completionHandler(response.data, nil)
                
            } catch  {
                let error = AJError(description: "Error parsing response from POST", kind: .SessionError)
                AJLogger.logError(error)
                completionHandler(nil, error)
                return
            }
        }
        
        task.resume()
    }
}

