//
//  AuthenticationService.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class AuthenticationService {
    
    static func getToken() -> String? {
        let defaults = UserDefaults.standard
        if let storedToken = defaults.string(forKey:"token") {
            return storedToken
        }
        return nil
    }
    
    static func setToken(token: String?) -> Void{
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "token")
    }

    func register(model: ToDictionaryProtocol, _ callback: @escaping (String?) -> Void) -> Void {
        let todosEndpoint = ServiceURLs.Account.registerUrl
        
        guard let todosURL = URL(string: todosEndpoint) else {
            let error = "Error: cannot create URL"
            AJLogger.logError(error)
            callback(error)
            return
        }
        var todosUrlRequest = URLRequest(url: todosURL)
        todosUrlRequest.httpMethod = "POST"
        
        let json = model.toDictionary()
        
        let jsonTodo: Data
        do {
            jsonTodo = try JSONSerialization.data(withJSONObject: json, options: [])
            todosUrlRequest.httpBody = jsonTodo
        } catch {
            let error = "Error: cannot create JSON"
            AJLogger.logError(error)
            callback(error)
            return
        }
        
        todosUrlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        todosUrlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: todosUrlRequest) {
            (data, response, error) in
            guard error == nil else {
                AJLogger.logError(error!.localizedDescription)
                callback(error!.localizedDescription)
                return
            }
            guard let responseData = data else {
                AJLogger.logError("Error: did not receive data")
                return
            }
            
          
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let responseRaw = try JSONSerialization.jsonObject(with: responseData,
                                                                          options: []) as? [String: Any] else {
                                                                            let error = "Could not get JSON from responseData as dictionary"
                                                                            AJLogger.logError(error)
                                                                            callback(error)
                                                                            return
                }

                let response = JsonResponse()
                response.load(dict: responseRaw)
                
                if response.status == ResponseStatuses.error{
                    callback(response.message)
                    return
                }
                
                callback(nil)
                
            } catch  {
                let error = "error parsing response from POST"
                AJLogger.logError(error)
                callback(error)
                return
            }
        }
        task.resume()
    }
    
    func login(username: String?, password: String?, _ callback: @escaping (String?) -> Void) -> Void {
        let todosEndpoint = ServiceURLs.Account.loginUrl
        
        guard let todosURL = URL(string: todosEndpoint) else {
            let error = "Error: cannot create URL"
            AJLogger.logError(error)
            callback(error)
            return
        }
        var todosUrlRequest = URLRequest(url: todosURL)
        todosUrlRequest.httpMethod = "POST"
        
        let json = "grant_type=password&username=\(username!)&password=\(password!)"
        todosUrlRequest.httpBody = json.data(using: .utf8)
        
        todosUrlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        todosUrlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: todosUrlRequest) {
            (data, response, error) in
            guard error == nil else {
                AJLogger.logError(error!.localizedDescription)
                callback(error!.localizedDescription)
                return
            }
            guard let responseData = data else {
                AJLogger.logError("Error: did not receive data")
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let responseRaw = try JSONSerialization.jsonObject(with: responseData,
                                                                         options: []) as? [String: Any] else {
                                                                            let error = "Could not get JSON from responseData as dictionary"
                                                                            AJLogger.logError(error)
                                                                            callback(error)
                                                                            return
                }
                
                AuthenticationService.setToken(token: responseRaw["access_token"] as! String?)
                
                //let error = responseRaw["error"] as! String?
                let error_desc = responseRaw["error_description"] as! String?
                if let lerror = error_desc {
                    callback(lerror)
                    return
                }
                
                callback(nil)
                
            } catch  {
                let error = "error parsing response from POST"
                AJLogger.logError(error)
                callback(error)
                return
            }
        }
        task.resume()
    }
    
    func getUserDetails(_ callback: @escaping (UserInfoResponse?, AJError?) -> Void) -> Void{
       
        SessionManager.dataTask(url: ServiceURLs.Account.getUserDetailsUrl, data: nil){
            if let errorParam = $1{
                callback(nil, AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            let model = UserInfoResponse()
            
            do {
                try model.load(data: $0 as Any)
            } catch {
                callback(nil, AJError(description: error.localizedDescription, kind: .DataParsing))
            }
            
            callback(model, nil)
        }
    }
    
    func updateUser(firstName: String?, lastName: String?, email: String?, password: String?, syncWithCalendar: Bool?, _ callback: @escaping (AJError?) -> Void){
        
        let request = UserRequest()
        request.email = email
        request.firstName = firstName
        request.lastName = lastName
        request.password = password
        request.syncWithCalendar = syncWithCalendar
        
        SessionManager.dataTask(url: ServiceURLs.Account.updateUserUrl, data: request){
            if let errorParam = $1{
                callback(AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            callback(nil)
        }
        
    }
    
    func resetPassword(email: String?, _ callback: @escaping (AJError?) -> Void){
        let request = ResetPasswordRequest()
        request.email = email
        
        SessionManager.dataTask(url: ServiceURLs.Account.resetPasswordUrl, data: request){
            if let errorParam = $1{
                callback(AJError(description: errorParam.localizedDescription, kind: .SessionError))
                return
            }
            
            callback(nil)
        }
    }
}
