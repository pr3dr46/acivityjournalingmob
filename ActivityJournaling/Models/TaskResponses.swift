//
//  TaskResponse.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/24/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation
class TaskDto: FromJsonProtocol{
    
    var id: Int? = 0
    var taskDate: Date? = nil
    var status: Int? = 0
    var subject: String? = nil
    var description: String? = nil
    var comment: String? = nil
    var emojiState: Int? = nil
    var syncWithCalendar: Bool? = nil
    var calendarEventIdentifier: String?
    var tags: [TagDto]? = nil
    
    func load(data: Any) throws {
        let responseRaw = data as! [String:Any?]
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        self.id = responseRaw["Id"] as? Int
        
        let td = responseRaw["TaskDate"] as? String
        
        if (td != nil){
            self.taskDate =  formatter.date(from: td!)
        }
        self.status = responseRaw["Status"] as? Int
        self.subject = responseRaw["Subject"] as? String
        self.description = responseRaw["Description"] as? String
        self.comment = responseRaw["Comment"] as? String
        self.calendarEventIdentifier = responseRaw["CalendarEventIdentifier"] as? String
        self.emojiState = responseRaw["EmojiState"] as? Int
        self.syncWithCalendar = responseRaw["SyncWithCalendar"] as? Bool
        
        let rawTags = responseRaw["Tags"] as? [Any]
        if rawTags != nil && rawTags!.count > 0{
            self.tags = [TagDto]()
            for rt in rawTags!{
                let tag = TagDto()
                try tag.load(data: rt)
                self.tags?.append(tag)
            }
        }
    }
}

class TagDto: FromJsonProtocol{
    
    var id: Int = 0
    var tagName: String? = nil
    var tagType: String? = nil
    var isSelected: Bool? = nil
    
    func load(data: Any) throws {
        let responseRaw = data as! [String:Any?]
        
        self.id = responseRaw["Id"] as! Int
        self.tagName = responseRaw["TagName"] as? String
        self.tagType = responseRaw["TagType"] as? String
        self.isSelected = responseRaw["IsSelected"] as? Bool
    }
}

class TasksStatusDto: FromJsonProtocol{
    var completed: Int? = nil
    var pending: Int? = nil
    var status: Int? = nil
    
    func load(data: Any) throws{
        let responseRaw = data as! [String: Any?]
        
        self.completed = responseRaw["CompletedTasks"] as? Int
        self.pending = responseRaw["PendingTasks"] as? Int
        self.status = responseRaw["Status"] as? Int
    }
}

class TaskSavedDto: FromJsonProtocol{
    var id: Int? = nil
    
    func load(data: Any) throws{
        let responseRaw = data as? Int
        self.id = responseRaw
    }
}
