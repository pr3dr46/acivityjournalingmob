//
//  AJError.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/24/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

struct AJError: Error{
    enum ErrorKind{
        case SessionError
        case DataParsing
    }
    
    let description: String?
    let kind: ErrorKind
    
    init(description: String?, kind: ErrorKind){
        self.description = description
        self.kind = kind
    }
}
