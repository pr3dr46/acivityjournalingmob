//
//  RegisterModel.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class RegistrationModel: ToDictionaryProtocol{
    var username: String?
    var password: String?
    var email: String?
    var firstName: String?
    var lastName: String?
    
    init(username: String?, password: String?, email: String?, firstName: String?, lastName: String?){
        self.username = username
        self.password = password
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
    }
    
    func toDictionary() -> Dictionary<String, String?> {
        let json = ["FirstName": self.firstName, "LastName": self.lastName, "Email": self.email, "Username": self.username, "Password": self.password, "ConfirmPassword": self.password] as Dictionary<String, String?>
        
        return json
    }
}

class UserRequest: ToUrlParametersProtocol{
    var username: String?
    var password: String?
    var email: String?
    var firstName: String?
    var lastName: String?
    var syncWithCalendar: Bool?
    
    
    func toParameters() -> String{
        var originalString = ""
        
        originalString = self.username != nil ? "username=\(self.username!)&" : "username=&"

        originalString += self.password != nil ? "password=\(self.password!)&" : "password=&"

        originalString += self.email != nil ? "email=\(self.email!)&" : "email=&"
        
        originalString += self.firstName != nil ? "firstName=\(self.firstName!)&" : "firstName=&"
        
        originalString += self.syncWithCalendar != nil ? "syncWithCalendar=\(self.syncWithCalendar!)&" : "syncWithCalendar=&"
        
        originalString += self.lastName != nil ? "lastName=\(self.lastName!)&" : "lastName=&"
        
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        return escapedString!
        
    }
    
}

class ResetPasswordRequest: ToUrlParametersProtocol{
    var email: String? = nil
    
    func toParameters() -> String {
        let originalString = self.email != nil ? "email=\(self.email!)&" : "email=&"
        
          let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        return escapedString!
    }
}
