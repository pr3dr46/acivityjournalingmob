//
//  DataRequests.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/28/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class SaveEntryRequest: ToUrlParametersProtocol{

    var id: Int? = nil
    var subject: String? = nil
    var entry: String? = nil
    var date: String? = nil
    var type: Int? = nil
    var tags: String? = nil
    
    func toParameters() -> String {
        var originalString = ""
        
        originalString = self.id != nil ? "id=\(self.id!)&" : "id=&"
        
        originalString += self.subject != nil ? "subject=\(self.subject!)&" : "subject=&"
        
        originalString += self.date != nil ? "date=\(self.date!)&" : "date=&"
        
        originalString += self.entry != nil ? "entry=\(self.entry!)&" : "entry=&"
        
        originalString += self.type != nil ? "type=\(self.type!)&" : "type=&"
        
        originalString += self.tags != nil ? "tags=\(self.tags!)&" : "tags="
        
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        return escapedString!
    }
}

class GetOrDeleteEntryRequest: ToUrlParametersProtocol{
    var id: Int? = nil
    var type: Int? = nil
    
    func toParameters() -> String {
        var originalString = self.id != nil ? "id=\(self.id!)&" : "id=&"
        originalString += self.type != nil ? "type=\(self.type!)&" : "type=&"
        return originalString
    }
}

class GetEntriesRequest : ToUrlParametersProtocol{
    
    var date: String?
    var type: Int?
    
    func toParameters() -> String {
        var originalString = self.date != nil ? "date=\(self.date!)&" : "date=&"
        
        originalString += self.type != nil ? "type=\(self.type!)&" : "type=&"
        
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        return escapedString!
    }
}
