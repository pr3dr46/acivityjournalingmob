//
//  Constatnts.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class ServiceURLs {
    static let serviceUrl = "https://api.springlify.com/"
    //static let serviceUrl = "http://192.168.0.10/"
    
    class Account {
        static let registerUrl = ServiceURLs.serviceUrl + "api/Account/Register"
        
        static let loginUrl = ServiceURLs.serviceUrl + "token"
        
        static let getUserDetailsUrl = ServiceURLs.serviceUrl + "api/Account/GetUserDetails"
        
        static let updateUserUrl = ServiceURLs.serviceUrl + "api/Account/UpdateUser"
        
        static let resetPasswordUrl = ServiceURLs.serviceUrl + "api/Account/SendResetPasswordEmail"
    }
    
    class Task{
        static let getTasksUrl = ServiceURLs.serviceUrl + "api/Task/GetTasks"
        
        static let getTaskUrl = ServiceURLs.serviceUrl + "api/Task/GetTask"
        
        static let saveTaskUrl = ServiceURLs.serviceUrl + "api/Task/SaveTask"
        
        static let deleteTaskUrl = ServiceURLs.serviceUrl + "api/Task/DeleteTask"
        
        static let updateStatusUrl = ServiceURLs.serviceUrl + "api/Task/UpdateStatus"
        
        static let getTasksStatus = ServiceURLs.serviceUrl + "api/Task/GetTasksStatus"
    }
    
    class Data{
        static let getEntriesUrl = ServiceURLs.serviceUrl + "api/Data/GetEntries"
        
        static let saveEntryUrl = ServiceURLs.serviceUrl + "api/Data/SaveEntry"
        
        static let deleteEntryUrl = ServiceURLs.serviceUrl + "api/Data/DeleteEntry"
        
        static let getEntryUrl = ServiceURLs.serviceUrl + "api/Data/GetEntry"
        
        static let getDataStatus = ServiceURLs.serviceUrl + "api/Data/GetDataStatus"
    }
}
