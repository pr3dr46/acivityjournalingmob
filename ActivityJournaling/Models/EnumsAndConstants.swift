//
//  Enums.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 7/11/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

enum ViewType{
    case Dashboard
    case Completed
    case NeedToDo
    case Struggle
    case Betterments
    case InterestingRead
    case NewStruggle
    case NewToDo
    case NewBetterments
    case NewInterestingRead
    case Settings
}

class ResponseStatuses{
    static let error = "error"
    static let ok = "ok"
}

class TaskStatuses{
    static let pending = 1
    static let done = 2
}

class DataTypes{
    static let struggle = 1
    static let beterrments = 2
    static let pointOfInterest = 3
}
