//
//  DataResponses.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/28/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class DataDto: FromJsonProtocol{
    
    var id: Int? = 0
    var date: Date? = nil
    var subject: String? = nil
    var entry: String? = nil
    var tags: [TagDto]? = nil
    
    func load(data: Any) throws {
        let responseRaw = data as! [String:Any?]
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        self.id = responseRaw["Id"] as? Int
        
        let td = responseRaw["Date"] as? String
        
        if (td != nil){
            self.date =  formatter.date(from: td!)
        }
        
        self.subject = responseRaw["Subject"] as? String
        self.entry = responseRaw["Entry"] as? String
        
        let rawTags = responseRaw["Tags"] as? [Any]
        if rawTags != nil && rawTags!.count > 0{
            self.tags = [TagDto]()
            for rt in rawTags!{
                let tag = TagDto()
                try tag.load(data: rt)
                self.tags?.append(tag)
            }
        }
    }
}

class DataStatusDto: FromJsonProtocol{
    var strugleTags: [String]? = nil
    var bettermentsTags: [String]? = nil
    var gratitudeTags: [String]? = nil
    
    func load(data: Any) throws {
        let responseRaw = data as! [String: Any?]
        self.strugleTags = responseRaw["StruggleTags"] as? [String]
        self.bettermentsTags = responseRaw["BettermentsTags"] as? [String]
        self.gratitudeTags = responseRaw["AreaOfGratitudeTags"] as? [String]
    }
    
    
    
    
}
