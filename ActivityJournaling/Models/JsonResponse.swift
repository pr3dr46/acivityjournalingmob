//
//  JsonResponse.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/20/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class JsonResponse{
    var status: String? = nil
    var message: String? = nil
    var data: Any? = nil
    
    func load(dict: Dictionary<String, Any>) -> Void {
        self.status = dict["Status"] as! String?
        self.message = dict["Message"] as! String?
        self.data = dict["Data"] as Any?
    }
}

