//
//  AuthenticationResponses.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/30/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation
class UserInfoResponse: FromJsonProtocol{
    var username: String?
    var email: String?
    var firstName: String?
    var lastName: String?
    var syncWithCalendar: Bool?
    
    func load(data: Any) throws {
        let responseRaw = data as! [String:Any?]
        
        self.username = responseRaw["Username"] as? String
        self.email = responseRaw["Email"] as? String
        self.firstName = responseRaw["FirstName"] as? String
        self.lastName = responseRaw["LastName"] as? String
        self.syncWithCalendar = responseRaw["SyncWithCalendar"] as? Bool
    }
}
