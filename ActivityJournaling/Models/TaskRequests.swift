//
//  TaskRequest.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/24/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

class GetTasksRequest : ToUrlParametersProtocol{

    var date: String?
    var status: Int?
    
    func toParameters() -> String {
        var originalString = self.date != nil ? "date=\(self.date!)&" : "date=&"
        
        originalString += self.status != nil ? "status=\(self.status!)" : "status="
        
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
      
        return escapedString!
    }
}

class GetOrDeleteTaskRequest: ToUrlParametersProtocol{
    var id: Int? = nil
    
    init(id: Int?){
        self.id = id
    }
    
    func toParameters() -> String {
        return self.id != nil ? "id=\(self.id!)" : "id="
    }
}

class UpdateTaskStatusRequest: ToUrlParametersProtocol{
    var id: Int? = nil
    var status: Int? = nil
    
    func toParameters() -> String {
        var query = self.id != nil ? "id=\(self.id!)&" : "id=&"
        query += self.status != nil ? "status=\(self.status!)" : "status="
        
        return query
    }
}


class SaveTaskRequest: ToUrlParametersProtocol{
    var id: Int? = nil
    var date: String? = nil
    var subject: String? = nil
    var description: String? = nil
    var tags: String? = nil
    var comment: String? = nil
    var emojiState: Int? = nil
    var calendarEventIdentifier: String?
    
    func toParameters() -> String {
      
        var originalString = ""
        
        originalString = self.id != nil ? "id=\(self.id!)&" : "id=&"
      
        originalString += self.subject != nil ? "subject=\(self.subject!)&" : "subject=&"
        
        originalString += self.date != nil ? "date=\(self.date!)&" : "date=&"
        
        originalString += self.description != nil ? "description=\(self.description!)&" : "description=&"
        
        originalString += self.tags != nil ? "tags=\(self.tags!)&" : "tags=&"
        
        originalString += self.calendarEventIdentifier != nil ? "calendarEventIdentifier=\(self.calendarEventIdentifier!)&" : "calendarEventIdentifier=&"
        
        originalString += self.emojiState != nil ? "emojiState=\(self.emojiState!)&" : "emojiState=&"
        
        originalString += self.comment != nil ? "comment=\(self.comment!)&" : "comment="
        
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        return escapedString!
    }
}
