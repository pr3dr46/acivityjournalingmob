//
//  ToJsonProtocol.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/20/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

protocol ToDictionaryProtocol:class {
    func toDictionary() -> Dictionary<String, String?>
}
