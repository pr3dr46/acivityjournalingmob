//
//  PassDataProtocol.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/26/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation
protocol PassDataProtocol: class{
    func Pass(data: Any?, sender: Sender)
}

enum Sender{
    case DatePicker
    case RegularPicker
}
