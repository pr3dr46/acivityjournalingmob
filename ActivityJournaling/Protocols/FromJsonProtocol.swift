//
//  FromJsonProtocol.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/24/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

protocol FromJsonProtocol: class{
    func load(data: Any) throws
}
