//
//  MenuItemTapDelegate.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 7/10/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

protocol MenuItemTapDelegate:class {
    func show(view: ViewType, data: Any?)
}
