//
//  ToUrlParametersProtocol.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/24/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation

protocol ToUrlParametersProtocol: class{
    func toParameters() -> String
}
