//
//  UIHelper.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/27/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation
import UIKit

class UIHelper{
    static let selectedTagViewColor: UIColor = UIColor.orange
    
    static func add(tags: [TagDto], toStackView: UIStackView) -> [UIView]{
        var width: CGFloat = 0
        let stackView = toStackView
        var views: [UIView] = [UIView]()
        
        for tag in tags{
            let view = UIView()
            view.layer.cornerRadius = 10.0
            view.clipsToBounds = true
            view.layer.borderWidth = 0.5
            view.heightAnchor.constraint(equalToConstant: 25).isActive = true
            view.translatesAutoresizingMaskIntoConstraints = false
            
            let label = UILabel()
            label.text = tag.tagName
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = label.font.withSize(12)
            
            let bottomSpace = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: label, attribute: .bottom, multiplier: 1, constant: 0)
            
            let topSpace = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: label, attribute: .top, multiplier: 1, constant: 0)
            
            let leftSpace = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 5)
            
            let rightSpace = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: label, attribute: .right, multiplier: 1, constant: 0)
            
            label.textColor = UIColor.darkGray
            
            view.addSubview(label)
            
            stackView.addArrangedSubview(view)
            label.sizeToFit()
           
            
            view.widthAnchor.constraint(equalToConstant: label.frame.width + 10).isActive = true
             view.sizeToFit()
            width += label.frame.width + 17
            
            NSLayoutConstraint.activate([bottomSpace, leftSpace, rightSpace, topSpace])
            
            view.tag = tag.id
            views.append(view)
            if tag.isSelected == true{
                view.backgroundColor = UIHelper.selectedTagViewColor
            }
        }

        for c in stackView.constraints{
            c.isActive = false
        }

        stackView.widthAnchor.constraint(equalToConstant: width).isActive = true
        stackView.layoutIfNeeded()
        
        return views
    }

}
