//
//  AlertDialog.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/20/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import Foundation
import UIKit

class AlertDialog {
    static func getErrorAlertDialog(_ text: String) -> UIAlertController {
        let alert = UIAlertController(title: "Error", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        return alert
    }
    
    static func getInfoAlertDialog(_ text: String) -> UIAlertController {
        let alert = UIAlertController(title: "Info", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        return alert
    }
    
    static func getConfirmDialog(_ text: String, _ callback: @escaping () -> Void) -> UIAlertController {
        let alert = UIAlertController(title: "Confirm", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            let arg = $0
            callback()
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        
        return alert
    }
}
