//
//  EntriesViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/28/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class EntriesViewController: UIViewController, UITableViewDataSource, PassDataProtocol {
    
    let dataService = DataService()
    
    var delegate: MenuItemTapDelegate? = nil
    
    @IBOutlet weak var dataTable: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    var items = [DataDto?]()
    var type: Int? = nil
    var datePickerVC: DatePickerModalViewController? = nil
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.rowHeight = 280
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "entryCell", for: indexPath) as! EntryTableViewCell
        
        let views = cell.stackView.arrangedSubviews
        for view in views{
            for sview in view.subviews{
                sview.removeFromSuperview()
            }
            cell.stackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        cell.stackView.layoutIfNeeded()
        
        let entry = items[indexPath.row]
        cell.initialize(id: entry!.id!, controller: self)
        cell.subject.text = entry?.subject
        cell.entryText.text = entry?.entry
        
        var tags = [TagDto]()
        if (entry?.tags != nil){
            for tag in (entry?.tags)!{
                tags.append(tag)
            }
        }
        
        cell.addTags(tags:tags)
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        if self.dateLabel.text == "nil"{
            self.dateLabel.text = formatter.string(from: Date())
        }
        
        self.loadData()
    }
    
    func loadData(){
        self.items.removeAll()
        self.dataTable!.reloadData()
        
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        self.dataService.getEntries(date: self.dateLabel.text, type: self.type){
            UIViewController.removeSpinner(spinner: spinner)
            
            let data = $0
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            if data != nil{
                for item in data!{
                    self.items.append(item)
                }
            }
            
            DispatchQueue.main.async(execute: {
                if self.dataTable != nil {
                    self.dataTable!.reloadData()
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func rightDateDidTap(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let date = formatter.date(from: self.dateLabel.text!)
        let previousDate = date?.addingTimeInterval(86400)
        
        self.dateLabel.text = formatter.string(from: previousDate!)
        
        self.loadData()
    }
    

    @IBAction func leftDateDidTap(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let date = formatter.date(from: self.dateLabel.text!)
        let previousDate = date?.addingTimeInterval(-86400)
        
        self.dateLabel.text = formatter.string(from: previousDate!)
        
        self.loadData()
    }
    
    
    @objc func taskNameTapped(sender:UITapGestureRecognizer){
        let label = sender.view as! UILabel
        label.textColor = UIColor.orange
        if self.type == DataTypes.beterrments{
            self.delegate?.show(view: .NewBetterments, data: label.tag)
        } else if self.type == DataTypes.struggle{
            self.delegate?.show(view: .NewStruggle, data: label.tag)
        }else{
            self.delegate?.show(view: .NewInterestingRead, data: label.tag)
        }
    }
    
    @objc func taskImageTapped(sender:UITapGestureRecognizer){
        let imageView = sender.view as! UIImageView
        
        if self.type == DataTypes.beterrments{
            self.delegate?.show(view: .NewBetterments, data: imageView.tag)
        } else if self.type == DataTypes.struggle{
            self.delegate?.show(view: .NewStruggle, data: imageView.tag)
        }else{
            self.delegate?.show(view: .NewInterestingRead, data: imageView.tag)
        }
    }
    
    
    func Pass(data: Any?, sender: Sender) {
        if sender == .DatePicker{
            let date = data as! String
            let formater = DateFormatter()
            formater.dateFormat = "dd/MM/yyyy"
            self.dateLabel.text = date
            self.loadData()
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let datePickerVC = segue.destination as? DatePickerModalViewController{
            self.datePickerVC = datePickerVC
            self.datePickerVC?.delegate = self
            self.datePickerVC?.dateMode = true
        }
    }
}

class EntryTableViewCell : UITableViewCell {
    
    var id: Int? = nil
    
    @IBOutlet weak var subject: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var entryText: UITextView!
    @IBOutlet weak var entryImageView: UIImageView!
    
    func initialize(id: Int, controller: EntriesViewController){
        self.id = id
        self.subject.tag = id
        self.entryImageView.tag = id
        let tapGesture = UITapGestureRecognizer.init(target: controller, action: #selector(EntriesViewController.taskNameTapped))
        self.subject.isUserInteractionEnabled = true
        self.subject.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer.init(target: controller, action: #selector(EntriesViewController.taskImageTapped))
        self.entryImageView.isUserInteractionEnabled = true
        self.entryImageView.addGestureRecognizer(tapGesture2)
    }

    func addTags(tags: [TagDto]){
        UIHelper.add(tags:tags, toStackView: self.stackView)
    }
}

