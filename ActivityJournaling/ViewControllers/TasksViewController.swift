//
//  CompletedViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 8/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class TasksViewController: UIViewController, UITableViewDataSource, PassDataProtocol{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    
    var items = [TaskDto?]()
    
    let taskService = TaskService()
    
    var delegate: MenuItemTapDelegate? = nil
    
    var status: Int? = nil
    
    var datePickerVC: DatePickerModalViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        if self.dateLabel.text == "nil"{
             self.dateLabel.text = formatter.string(from: Date())
        }
       
        self.loadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.rowHeight = 80
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! CompletedTableViewCell

        let views = cell.stackView.arrangedSubviews
        for view in views{
            for sview in view.subviews{
                sview.removeFromSuperview()
            }
            cell.stackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        cell.stackView.layoutIfNeeded()
        
        
        let task = items[indexPath.row]
        cell.initialize(id: task!.id!, controller: self)
        cell.taskName.text = task?.subject
        cell.setEmoji(state: task?.emojiState)
        
        var tags = [TagDto]()
        if (task?.tags != nil){
            for tag in (task?.tags)!{
                tags.append(tag)
            }
        }
        
        cell.addTags(tags:tags)
        
        return cell
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        self.items.removeAll()
        self.tableView!.reloadData()
        
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        self.taskService.getTasks(date: self.dateLabel.text, status: self.status){
            UIViewController.removeSpinner(spinner: spinner)
            
            let data = $0
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            if data != nil{
                for item in data!{
                    self.items.append(item)
                }
            }
            
            DispatchQueue.main.async(execute: {
                if self.tableView != nil {
                    self.tableView!.reloadData()
                }
            })
        }
    }
    
    
    @objc func taskNameTapped(sender:UITapGestureRecognizer){
        let label = sender.view as! UILabel
        label.textColor = UIColor.orange
        self.delegate?.show(view: .NewToDo, data: label.tag)
    }
    
    @objc func taskImageTapped(sender:UITapGestureRecognizer){
        let image = sender.view as! UIImageView
        self.delegate?.show(view: .NewToDo, data: image.tag)
    }
    
    @IBAction func leftDateDidTap(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let date = formatter.date(from: self.dateLabel.text!)
        let previousDate = date?.addingTimeInterval(-86400)
        
        self.dateLabel.text = formatter.string(from: previousDate!)

        self.loadData()
    }
    
    @IBAction func rightDateDidTap(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let date = formatter.date(from: self.dateLabel.text!)
        let previousDate = date?.addingTimeInterval(86400)
        
        self.dateLabel.text = formatter.string(from: previousDate!)
        
        self.loadData()
        
    }
    
    func Pass(data: Any?, sender: Sender) {
        if sender == .DatePicker{
            let date = data as! String
            self.dateLabel.text = date
            self.loadData()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let datePickerVC = segue.destination as? DatePickerModalViewController{
            self.datePickerVC = datePickerVC
            self.datePickerVC?.delegate = self
            self.datePickerVC?.dateMode = true
        }
    }
}

class CompletedTableViewCell : UITableViewCell {
    
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var taskImage: UIImageView!
    
    @IBOutlet weak var stackView: UIStackView!
    
    var isInitialized: Bool = false
    var id: Int = 0
    
    @IBOutlet weak var emoji: UIImageView!
    
    func initialize(id: Int, controller: TasksViewController){
        self.isInitialized = true
        self.id = id
        self.taskName.tag = id
        self.taskImage.tag = id
        let tapGesture = UITapGestureRecognizer.init(target: controller, action: #selector(TasksViewController.taskNameTapped))
        self.taskName.isUserInteractionEnabled = true
        self.taskName.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer.init(target: controller, action: #selector(TasksViewController.taskImageTapped))
        self.taskImage.isUserInteractionEnabled = true
        self.taskImage.addGestureRecognizer(tapGesture2)
    }

    func addTags(tags: [TagDto]){
        UIHelper.add(tags:tags, toStackView: self.stackView)
    }
    
    func setEmoji(state: Int?){
        
        if state == 1{
            self.emoji.image = UIImage(named: "emoji-1.png")
        }
        if state == 2{
           self.emoji.image = UIImage(named: "emoji-2.png")
        }
        if state == 3{
           self.emoji.image = UIImage(named: "emoji-3.png")
        }
        if state == 4{
           self.emoji.image = UIImage(named: "emoji-4.png")
        }
        if state == nil{
            self.emoji.isHidden = true
        }else{
            
            self.emoji.isHidden = false
        }
        
    }
}
