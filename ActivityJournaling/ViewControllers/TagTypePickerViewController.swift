//
//  TagTypePickerViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 10/8/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class TagTypePickerViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate {

    var delegate: PassDataProtocol?
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var picker: UIPickerView!
    
    var tagTypes: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dialogView.layer.cornerRadius = 10
        self.picker.setValue(UIColor.white, forKey: "textColor")
        
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.tagTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.tagTypes[row]
    }

    @IBAction func closeDidTap(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }

    @IBAction func okDidTap(_ sender: Any) {
        let row = self.picker.selectedRow(inComponent: 0)
        self.delegate?.Pass(data: self.tagTypes[row], sender: .RegularPicker)
          self.dismiss(animated: true, completion: nil)
    }
    
    func refreshPicker() -> Void{
        self.picker.reloadAllComponents()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
