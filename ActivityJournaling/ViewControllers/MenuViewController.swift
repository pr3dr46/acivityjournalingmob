//
//  MenuViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 7/10/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class MenuViewController: UITableViewController {
  
    weak var delegate: MenuItemTapDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && indexPath.section == 0{
            self.delegate?.show(view: .Dashboard, data: nil)
        } else if indexPath.row == 1 && indexPath.section == 0{
            self.delegate?.show(view: .Completed, data: nil)
        } else if indexPath.row == 2 && indexPath.section == 0{
            self.delegate?.show(view: .NeedToDo, data: nil)
        } else if indexPath.row == 3 && indexPath.section == 0{
            self.delegate?.show(view: .Struggle, data: nil)
        } else if indexPath.row == 4 && indexPath.section == 0{
            self.delegate?.show(view: .Betterments, data: nil)
        } else if indexPath.row == 5 && indexPath.section == 0{
            self.delegate?.show(view: .InterestingRead, data: nil)
        }else if indexPath.row == 6 && indexPath.section == 0{
            self.delegate?.show(view: .Settings, data: nil)
        }else if indexPath.row == 0 && indexPath.section == 1{
            self.delegate?.show(view: .NewToDo, data: nil)
        }else if indexPath.row == 1 && indexPath.section == 1{
            self.delegate?.show(view: .NewStruggle, data: nil)
        }else if indexPath.row == 2 && indexPath.section == 1{
            self.delegate?.show(view: .NewBetterments, data: nil)
        }else if indexPath.row == 3 && indexPath.section == 1{
            self.delegate?.show(view: .NewInterestingRead, data: nil)
        }
        
        dismiss(animated: true, completion: nil)
    }

    //@IBAction func dashboardDidTap(_ sender: Any) {
        //self.delegate?.show(view: .Dashboard)
    //}
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
