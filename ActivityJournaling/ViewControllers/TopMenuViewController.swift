//
//  TopMenuViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 8/18/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class TopMenuViewController: UIViewController, MenuItemTapDelegate {
    
    weak var delegate: MenuItemTapDelegate?
    
    func show(view: ViewType, data: Any?) {
        delegate?.show(view: view, data: nil)
    }
    
    var navController: MenuViewController? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? UINavigationController{
            let menuController = dest.viewControllers.first as? MenuViewController
            menuController?.delegate = self
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
