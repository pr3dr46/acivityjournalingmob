//
//  DailyDidViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 7/12/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class DailyDidTableViewCell : UITableViewCell{
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var tag1View: UIView!
    @IBOutlet weak var tag2View: UIView!
    @IBOutlet weak var tag3View: UIView!

}

class CompletedViewController: UIViewController, UITableViewDataSource {
    let tasks = ["New release","Order meds","New blog post - don't panic!", "Fixsession with Ken", "Eye test"]
    let subs = ["!!! Business", "! Health","!! Marketing", "! Football", "! Health"]
    
    
    @IBOutlet weak var dailyDidView: UIView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! DailyDidTableViewCell
        
        cell.cellLabel.text = tasks[indexPath.item]
      
        cell.tag1View.layer.cornerRadius = 10
        cell.tag1View.layer.borderWidth = 0.5
        cell.tag1View.clipsToBounds = true
        
        cell.tag2View.layer.cornerRadius = 10
        cell.tag2View.layer.borderWidth = 0.5
        cell.tag2View.clipsToBounds = true
        
        cell.tag3View.layer.cornerRadius = 10
        cell.tag3View.layer.borderWidth = 0.5
        cell.tag3View.clipsToBounds = true
        
        tableView.rowHeight = 80
        
        return cell
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dailyDidView.layer.cornerRadius = 10
        self.dailyDidView.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
