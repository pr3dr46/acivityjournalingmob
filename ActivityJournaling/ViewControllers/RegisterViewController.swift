//
//  RegisterViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {

    let authService: AuthenticationService = AuthenticationService()
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.password.isSecureTextEntry = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveDidTap(_ sender: Any) {

        let model = RegistrationModel(username: username?.text, password: password?.text, email: email?.text, firstName: firstName?.text, lastName: lastName?.text)
        
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        self.authService.register(model: model) {
            UIViewController.removeSpinner(spinner: spinner)
            
            if let error = $0{
                let dlg = AlertDialog.getErrorAlertDialog(error)
                DispatchQueue.main.async(execute: {
                  self.present(dlg, animated: true, completion: nil)
                })
                
                return
            }
            
            let storyboard = UIStoryboard(name: "LoginScreen", bundle: nil)
            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerIdentifier")
            
            DispatchQueue.main.async(execute: {
                self.present(loginVC, animated: true, completion: nil)
            })
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
