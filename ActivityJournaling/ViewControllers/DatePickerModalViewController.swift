//
//  DatePickerViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 8/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class DatePickerModalViewController: UIViewController {

    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    var dateMode = false
    
    var delegate: PassDataProtocol?
    
    override func viewDidLoad() {

        super.viewDidLoad()
        self.datePickerView.layer.cornerRadius = 10
        self.datePicker.setValue(UIColor.white, forKey: "textColor")
        if self.dateMode{
            self.datePicker.datePickerMode = .date
        }else{
            self.datePicker.datePickerMode = .dateAndTime
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func closeDidTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okDidTap(_ sender: Any) {
        let formatter = DateFormatter()
        if dateMode == true{
            formatter.dateFormat = "MM/dd/yyyy"
        }else{
            formatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        }
        let date = formatter.string(from: self.datePicker.date)
        self.delegate?.Pass(data: date, sender: .DatePicker)
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
