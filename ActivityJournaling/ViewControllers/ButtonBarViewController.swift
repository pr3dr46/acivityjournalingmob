//
//  ButtonBarViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 8/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class ButtonBarViewController: UIViewController {

    weak var delegate: MenuItemTapDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func newToDoButtonTap(_ sender: UIButton) {
        delegate?.show(view: .NewToDo, data: nil)
    }
    
    
    @IBAction func newStruggleButtonTap(_ sender: UIButton) {
        delegate?.show(view: .NewStruggle, data: nil)
    }
    
    
    @IBAction func needToChangeButtonDidTap(_ sender: UIButton) {
        delegate?.show(view: .NewBetterments,data: nil)
    }
    
    @IBAction func interestingReadButtonDidTap(_ sender: UIButton) {
        delegate?.show(view: .NewInterestingRead, data: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
