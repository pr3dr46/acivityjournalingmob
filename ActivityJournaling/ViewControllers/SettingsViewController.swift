//
//  SettingsViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 8/19/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {
    let authService = AuthenticationService()
    
    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var syncWithCalendar: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        self.authService.getUserDetails(){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            let model = $0
            DispatchQueue.main.async(execute: {
               self.firstName.text = model?.firstName
               self.lastName.text = model?.lastName
               self.email.text = model?.email
               self.syncWithCalendar.isOn = model?.syncWithCalendar == true
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveChangesDidTap(_ sender: Any) {
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        self.authService.updateUser(firstName: self.firstName.text, lastName: self.lastName.text, email: self.email.text, password: self.password.text, syncWithCalendar: self.syncWithCalendar.isOn){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $0 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
        }
    }
    
    @IBAction func logoutDidTap(_ sender: Any) {
        AuthenticationService.setToken(token: nil)
        let storyboard = UIStoryboard(name: "LoginScreen", bundle: nil)
        let ctrl = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerIdentifier")
        self.present(ctrl, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
