//
//  ApplicationViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 7/10/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class ApplicationViewController: UIViewController, MenuItemTapDelegate {
    
    weak var navigationVC: UINavigationController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenStoryboard = UIStoryboard(name: "AppScreens", bundle: nil)
        let vc = screenStoryboard.instantiateViewController(withIdentifier: "DashboardViewControllerIdentifier") as! DashboardViewController
        vc.delegate = self
        self.navigationVC!.pushViewController(vc, animated: true)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func show(view: ViewType, data: Any?) {
        self.navigationVC!.didMove(toParentViewController: self)
        
        let screenStoryboard = UIStoryboard(name: "AppScreens", bundle: nil)
        
        switch view {
            case .Completed:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "CompletedViewControllerIdentifier") as! TasksViewController
                vc.delegate = self
                vc.status = TaskStatuses.done
                self.navigationVC!.pushViewController(vc, animated: true)
            case .Dashboard:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "DashboardViewControllerIdentifier") as! DashboardViewController
                vc.delegate = self
                self.navigationVC!.pushViewController(vc, animated: true)
            case .InterestingRead:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "EntriesViewControllerIdentifier") as! EntriesViewController
                vc.type = DataTypes.pointOfInterest
                vc.delegate = self
                self.navigationVC!.pushViewController(vc, animated: true)
            case .Betterments:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "EntriesViewControllerIdentifier") as! EntriesViewController
                vc.type = DataTypes.beterrments
                vc.delegate = self
                self.navigationVC!.pushViewController(vc, animated: true)
            case .Struggle:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "EntriesViewControllerIdentifier") as! EntriesViewController
                vc.type = DataTypes.struggle
                vc.delegate = self
                self.navigationVC!.pushViewController(vc, animated: true)
            case .NeedToDo:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "CompletedViewControllerIdentifier") as! TasksViewController
                vc.delegate = self
                vc.status = TaskStatuses.pending
                self.navigationVC!.pushViewController(vc, animated: true)
            case .NewToDo:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "NewToDoViewControllerIdentifier") as! NewToDoViewController
                if data != nil{
                    vc.taskId = data as? Int
                }
                self.navigationVC!.pushViewController(vc, animated: true)
            case .NewInterestingRead:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "NewEntryViewControllerIdentifier") as! NewEntryViewController
                vc.viewType = view
                vc.type = DataTypes.pointOfInterest
                if data != nil{
                    vc.id = data as? Int
                }
                self.navigationVC!.pushViewController(vc, animated: true)
            case .NewBetterments:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "NewEntryViewControllerIdentifier") as! NewEntryViewController
                vc.viewType = view
                vc.type = DataTypes.beterrments
                if data != nil{
                    vc.id = data as? Int
                }
                self.navigationVC!.pushViewController(vc, animated: true)
            case .NewStruggle:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "NewEntryViewControllerIdentifier") as! NewEntryViewController
                vc.viewType = view
                vc.type = DataTypes.struggle
                if data != nil{
                    vc.id = data as? Int
                }
                self.navigationVC!.pushViewController(vc, animated: true)
            case .Settings:
                let vc = screenStoryboard.instantiateViewController(withIdentifier: "SettingsViewControllerIdentifier")
                self.navigationVC!.pushViewController(vc, animated: true)
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let menuVC = segue.destination as? TopMenuViewController{
            menuVC.delegate = self
        }
       
        if let navVC = segue.destination as? UINavigationController{
            self.navigationVC = navVC
        }

        
        if let barVC = segue.destination as? ButtonBarViewController{
            barVC.delegate = self
        }
    }

}
