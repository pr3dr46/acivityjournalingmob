//
//  NewToDoViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 7/12/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class NewToDoViewController: BaseViewController, PassDataProtocol, UITextViewDelegate {

    var taskId: Int? = nil
    
    let taskService = TaskService()
    var datePickerVC: DatePickerModalViewController?
    var tagPickerVC: TagTypePickerViewController?
    var selectedTags: [Int] = [Int]()
    var tagViews: [UIView] = [UIView]()
    var selectedEmoji: Int? = nil
    var tagTypes: [String] = ["All Types"]
    var selectedTagType: String? = nil
    var syncWithCalendar: Bool? = nil
    var eventIdentifier: String? = nil
    var allTags: [TagDto] = [TagDto]()
    
    @IBOutlet weak var subject: UITextField!
    @IBOutlet weak var taskDescription: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var newToDoView: UIView!
    @IBOutlet weak var tagsStackView: UIStackView!
    @IBOutlet weak var comments: UITextView!
    @IBOutlet weak var completed: UISwitch!
    
    @IBOutlet weak var emoji1: UIImageView!
    @IBOutlet weak var emoji2: UIImageView!
    @IBOutlet weak var emoji3: UIImageView!
    @IBOutlet weak var emoji4: UIImageView!
    
    
    @IBOutlet weak var pickTagTypeBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(NewToDoViewController.emojiTapped))
        
        self.emoji1.addGestureRecognizer(tapGestureRecognizer1)
        self.emoji1.isUserInteractionEnabled = true
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(NewToDoViewController.emojiTapped))
        
        self.emoji2.addGestureRecognizer(tapGestureRecognizer2)
        self.emoji2.isUserInteractionEnabled = true
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(NewToDoViewController.emojiTapped))
        
        self.emoji3.addGestureRecognizer(tapGestureRecognizer3)
        self.emoji3.isUserInteractionEnabled = true
        
        let tapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(NewToDoViewController.emojiTapped))
        
        self.emoji4.addGestureRecognizer(tapGestureRecognizer4)
        self.emoji4.isUserInteractionEnabled = true
        
        self.comments.delegate = self
        
        let spinner = UIViewController.displaySpinner(onView: self.view)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        self.dateLabel.text = formatter.string(from: Date())
        
        self.taskService.getTask(id: self.taskId){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            let task = $0
            let tags = task?.tags
            
            self.selectedTags = [Int]()
            
            DispatchQueue.main.async(execute: {
                
                self.taskId = task?.id
                self.comments.text = task?.comment
                self.subject.text = task?.subject
                self.taskDescription.text = task?.description
                self.completed.isEnabled = self.taskId != nil
                self.completed.isOn = task?.status == TaskStatuses.done
                self.syncWithCalendar = task?.syncWithCalendar
                self.eventIdentifier = task?.calendarEventIdentifier
                
                if task?.taskDate != nil{
                    self.dateLabel.text = formatter.string(from: (task?.taskDate)!)
                }
                self.setEmoji(state: task?.emojiState)
                
                if tags != nil{
                    self.allTags = task!.tags!
                    self.tagViews = UIHelper.add(tags: tags!, toStackView: self.tagsStackView)
                    for view in self.tagViews{
                        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(NewToDoViewController.tagViewTapped))
                        view.isUserInteractionEnabled = true
                        view.addGestureRecognizer(tapGesture)
                    }
                    
                    for tag in tags!{
                        if tag.isSelected == true{
                            self.selectedTags.append(tag.id)
                        }
                        
                        if self.tagTypes.index(of: tag.tagType!) != nil{
                            continue
                        }else{
                            self.tagTypes.append(tag.tagType!)
                        }
                    }
                    
                }
            })
        }
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
         self.view.frame.origin.y = -150
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
          self.view.frame.origin.y = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didSaveTap(_ sender: Any) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        let startDate = formatter.date(from: self.dateLabel.text!)
        let endDate = startDate!.addingTimeInterval(3600)
        
        if self.syncWithCalendar == true {
            self.taskService.addEventToCalendar(identifier: self.eventIdentifier, title: self.subject.text!, description: self.taskDescription.text, startDate: startDate!, endDate: endDate){
                if let errorParam = $1 {
                    let dlg = AlertDialog.getErrorAlertDialog(errorParam.localizedDescription)
                    DispatchQueue.main.async(execute: {
                        self.present(dlg, animated: true, completion: nil)
                    })
                }
                
                self.eventIdentifier = $0
                
                self.saveTask()
            }
        }else{
            self.saveTask()
        }
    }
    
    @IBAction func didDeleteTap(_ sender: Any) {
        if self.taskId == nil{
            let dlg = AlertDialog.getInfoAlertDialog("Please, save task before taking this action.")
            self.present(dlg, animated: true, completion: nil)
            return
        }
        
        let dlg = AlertDialog.getConfirmDialog("You are going to delete task. Are you sure?"){
            
            let spinner = UIViewController.displaySpinner(onView: self.view)

            self.taskService.deleteTask(id: self.taskId){
                UIViewController.removeSpinner(spinner: spinner)
                
                if let errorParam = $0 {
                    let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                    DispatchQueue.main.async(execute: {
                        self.present(dlg, animated: true, completion: nil)
                    })
                    return
                }
                
                DispatchQueue.main.async(execute: {
                    self.taskId = nil
                    self.comments.text = nil
                    self.subject.text = nil
                    self.taskDescription.text = nil
                    self.selectedTags = [Int]()
                    self.setEmoji(state: nil)
                    
                    for view in self.tagViews{
                        view.backgroundColor = UIColor.white
                    }
                })
                
                if let identifier = self.eventIdentifier{
                    self.taskService.removeEventFromCalendar(identifier: identifier)
                }
            }
        }
        self.present(dlg, animated: true, completion: nil)
    }
    
    @objc func emojiTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.selectedEmoji = tappedImage.tag
        var shouldClear = false
        if (tappedImage.backgroundColor != UIColor.clear){
            shouldClear = true
            self.selectedEmoji = nil
        }
        
        self.emoji1.backgroundColor = UIColor.clear
        self.emoji2.backgroundColor = UIColor.clear
        self.emoji3.backgroundColor = UIColor.clear
        self.emoji4.backgroundColor = UIColor.clear
        
        if (!shouldClear){
            tappedImage.backgroundColor = UIColor.lightGray
        }
    }
    
    func saveTask() -> Void{
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        let ids = (self.selectedTags.map { String($0) }).joined(separator: ",");
        
        let emojiState = self.getEmojiState()
        
        self.taskService.saveTask(id: self.taskId, subject: self.subject.text, description: self.taskDescription.text, date: self.dateLabel.text, tags: ids, comment: self.comments.text, emojiState: emojiState, eventIdentifier: self.eventIdentifier){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                
            }
            
            let task = $0
            self.taskId = task?.id

            DispatchQueue.main.async(execute: {
                let dlg = AlertDialog.getInfoAlertDialog("Task is successfully saved")
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                self.completed.isEnabled = self.taskId != nil
            })
        }
    }
    
    func getEmojiState() -> Int?{
        return self.selectedEmoji
    }
    
    func setEmoji(state: Int?){
        self.emoji1.backgroundColor = UIColor.clear
        self.emoji2.backgroundColor = UIColor.clear
        self.emoji3.backgroundColor = UIColor.clear
        self.emoji4.backgroundColor = UIColor.clear
        
        self.selectedEmoji = state
        
        if state == 1{
            self.emoji1.backgroundColor = UIColor.lightGray
        }
        if state == 2{
            self.emoji2.backgroundColor = UIColor.lightGray
        }
        if state == 3{
            self.emoji3.backgroundColor = UIColor.lightGray
        }
        if state == 4{
            self.emoji4.backgroundColor = UIColor.lightGray
        }
    }

    func Pass(data: Any?, sender: Sender) {
        if sender == .DatePicker{
            let date = data as! String
            self.dateLabel.text = date
        }else{
            self.selectedTagType = data as? String
            var filtered = [TagDto]()
            
            for tag in self.allTags{
                if tag.tagType == self.selectedTagType || self.selectedTagType == "All Types"{
                    filtered.append(tag)
                    tag.isSelected = self.selectedTags.index(of: tag.id) != nil
                }
            }
            
            for view in self.tagViews{
                for sview in view.subviews{
                    sview.removeFromSuperview()
                }
                self.tagsStackView.removeArrangedSubview(view)
                view.removeFromSuperview()
            }
            self.tagsStackView.layoutIfNeeded()
            
            self.tagViews = UIHelper.add(tags: filtered, toStackView: self.tagsStackView)
            for view in self.tagViews{
                let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(NewToDoViewController.tagViewTapped))
                view.isUserInteractionEnabled = true
                view.addGestureRecognizer(tapGesture)
            }
            
            self.pickTagTypeBtn.setTitle(self.selectedTagType, for: .normal)
        }
    }
    
    @objc func tagViewTapped(sender:UITapGestureRecognizer){
        let tagView = sender.view! as UIView
        
        if tagView.backgroundColor == UIHelper.selectedTagViewColor{
            tagView.backgroundColor = UIColor.white
            let index = self.selectedTags.index(of: tagView.tag)!
            self.selectedTags.remove(at: index)
        }else{
            tagView.backgroundColor = UIHelper.selectedTagViewColor
            self.selectedTags.append(tagView.tag)
        }
        
        tagsStackView.layoutIfNeeded()
    }

    @IBAction func completedDidChange(_ sender: Any) {
        let status = self.completed.isOn ? 2 : 1
        
        self.taskService.updateTaskStatus(id: self.taskId, status: status){
            
            if let errorParam = $0 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let datePickerVC = segue.destination as? DatePickerModalViewController{
            self.datePickerVC = datePickerVC
            self.datePickerVC?.delegate = self
        }
        
        if let tagPickerVC = segue.destination as? TagTypePickerViewController{
            self.tagPickerVC = tagPickerVC
            self.tagPickerVC?.delegate = self
            self.tagPickerVC?.tagTypes = self.tagTypes
        }
    }
}
