//
//  DashboardViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 7/12/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit
import SwiftChart

class DashboardViewController: UIViewController {
    
    let dataService = DataService()
    let taskService = TaskService()
    
    @IBOutlet weak var weeklyTasksView: UIView!
    @IBOutlet weak var savedTasksView: UIView!
    @IBOutlet weak var struggleViewAll: UIView!
    @IBOutlet weak var gratitudeViewAll: UIView!
    
    @IBOutlet weak var ntcViewAll: UIView!
    
    @IBOutlet weak var plannedTasks: UILabel!
    @IBOutlet weak var completedTasks: UILabel!
    @IBOutlet weak var strugleStackView: UIStackView!
    @IBOutlet weak var bettermentsStackView: UIStackView!
    @IBOutlet weak var gratitudeStackView: UIStackView!
    
    var delegate: MenuItemTapDelegate? = nil
    
    @IBOutlet weak var emoji: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        
        self.setUpTopBarsLayout()
        self.setUpStruggleItems()
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        
        self.dataService.getDataStatus(){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            let status = $0
            
            
            DispatchQueue.main.async(execute: {
                var btags = [TagDto]()
                for btag in status!.bettermentsTags!{
                    let tag = TagDto()
                    tag.tagName = btag
                    btags.append(tag)
                }
                
                var stags = [TagDto]()
                for stag in status!.strugleTags!{
                    let tag = TagDto()
                    tag.tagName = stag
                    stags.append(tag)
                }
                
                var gtags = [TagDto]()
                for gtag in status!.gratitudeTags!{
                    let tag = TagDto()
                    tag.tagName = gtag
                    gtags.append(tag)
                }
                
                UIHelper.add(tags: btags, toStackView: self.bettermentsStackView)
                UIHelper.add(tags: stags, toStackView: self.strugleStackView)
                UIHelper.add(tags:gtags, toStackView: self.gratitudeStackView)
                
            })
            
        }
        
        self.taskService.getTasksStatus(){
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            let status = $0
            
            DispatchQueue.main.async(execute: {
                self.plannedTasks.text = String(status!.pending!)
                self.completedTasks.text = String(status!.completed!)
                if status!.status == 1{
                    self.emoji.image = UIImage(named: "emoji-1.png")
                } else if status!.status == 2{
                    self.emoji.image = UIImage(named: "emoji-2.png")
                } else if status!.status == 3{
                    self.emoji.image = UIImage(named: "emoji-3.png")
                } else if status!.status == 4{
                    self.emoji.image = UIImage(named: "emoji-4.png")
                }
                
                self.emoji.contentMode = UIViewContentMode.scaleToFill
            
            })
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpTopBarsLayout(){
        self.savedTasksView.layer.cornerRadius = 5.0
    
        self.savedTasksView.clipsToBounds = true
        
        self.weeklyTasksView.layer.cornerRadius = 5.0
      
        self.weeklyTasksView.clipsToBounds = true
    }

    func setUpStruggleItems(){
        
        self.struggleViewAll.layer.cornerRadius = 15.0
        self.struggleViewAll.layer.borderWidth = 0.5
        self.struggleViewAll.clipsToBounds = true
        self.struggleViewAll.layer.borderColor = UIColor.lightGray.cgColor
        
        self.ntcViewAll.layer.cornerRadius = 15.0
        self.ntcViewAll.layer.borderWidth = 0.5
        self.ntcViewAll.clipsToBounds = true
        self.ntcViewAll.layer.borderColor = UIColor.lightGray.cgColor
        
        self.gratitudeViewAll.layer.cornerRadius = 15.0
        self.gratitudeViewAll.layer.borderWidth = 0.5
        self.gratitudeViewAll.clipsToBounds = true
        self.gratitudeViewAll.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func viewAllStruggles(_ sender: Any) {
        self.delegate?.show(view: .Struggle, data: nil)
    }
    
    @IBAction func viewAllBettermentsDidTap(_ sender: Any) {
        self.delegate?.show(view: .Betterments, data: nil)
    }
    
    @IBAction func viewAllGratitude(_ sender: Any) {
        self.delegate?.show(view: .InterestingRead, data: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
