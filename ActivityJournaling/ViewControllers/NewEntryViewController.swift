//
//  NewEntryViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 9/28/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class NewEntryViewController: BaseViewController, PassDataProtocol {
   
    var id: Int? = nil
    let dataService = DataService()
    var viewType: ViewType? = nil
    var datePickerVC: DatePickerModalViewController?
    var type: Int?
    var tagViews: [UIView] = [UIView]()
    var selectedTags: [Int] = [Int]()
    var selectedTagType: String? = nil
    var allTags: [TagDto] = [TagDto]()
    var tagTypes: [String] = ["All Types"]
    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var subject: UITextField!
    @IBOutlet weak var tagsStackView: UIStackView!
    @IBOutlet weak var journalEntry: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tagTypesBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.viewType == ViewType.NewBetterments{
            self.header.text = "Betterments"
            self.type = DataTypes.beterrments
        }else if self.viewType == ViewType.NewStruggle{
            self.header.text = "Struggles"
            self.type = DataTypes.struggle
        }else{
            self.header.text = "Area of Gratitude"
            self.type = DataTypes.pointOfInterest
        }
        
        let spinner = UIViewController.displaySpinner(onView: self.view)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.dateLabel.text = formatter.string(from: Date())
        
        self.dataService.getEntry(id: self.id, type: self.type){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $1 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            let entry = $0
            let tags = entry?.tags
            
            self.selectedTags = [Int]()
            
            DispatchQueue.main.async(execute: {
                
                self.id = entry?.id
                self.subject.text = entry?.subject
                self.journalEntry.text = entry?.entry
                if entry?.date != nil{
                    self.dateLabel.text = formatter.string(from: (entry?.date)!)
                }
                
                if tags != nil{
                    
                    self.allTags = tags!
                    
                    self.tagViews = UIHelper.add(tags: tags!, toStackView: self.tagsStackView)
                    for view in self.tagViews{
                        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(NewToDoViewController.tagViewTapped))
                        view.isUserInteractionEnabled = true
                        view.addGestureRecognizer(tapGesture)
                    }
                    
                    for tag in tags!{
                        if tag.isSelected == true{
                            self.selectedTags.append(tag.id)
                        }
                        
                        if self.tagTypes.index(of: tag.tagType!) != nil{
                            continue
                        }else{
                            self.tagTypes.append(tag.tagType!)
                        }
                    }
                   
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteDidTap(_ sender: Any) {
        if self.id == nil{
            let dlg = AlertDialog.getInfoAlertDialog("Please, save task before taking this action.")
            self.present(dlg, animated: true, completion: nil)
            return
        }
        
        let dlg = AlertDialog.getConfirmDialog("You are going to delete entry. Are you sure?"){
            
            let spinner = UIViewController.displaySpinner(onView: self.view)
            
            self.dataService.deleteEntry(id: self.id, type: self.type){
                UIViewController.removeSpinner(spinner: spinner)
                
                if let errorParam = $0 {
                    let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                    DispatchQueue.main.async(execute: {
                        self.present(dlg, animated: true, completion: nil)
                    })
                    return
                }
                
                DispatchQueue.main.async(execute: {
                    self.id = nil
                    self.journalEntry.text = nil
                    self.subject.text = nil
                    self.selectedTags = [Int]()
                    
                    for view in self.tagViews{
                        view.backgroundColor = UIColor.white
                    }
                })
            }
        }
        self.present(dlg, animated: true, completion: nil)
    }
    
    @IBAction func saveDidTap(_ sender: Any) {
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        let ids = (self.selectedTags.map { String($0) }).joined(separator: ",");
        
        self.dataService.saveEntry(id: self.id, subject: self.subject.text, entry: self.journalEntry.text, date: self.dateLabel.text, tags: ids, type: self.type){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $0 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            DispatchQueue.main.async(execute: {
                let dlg = AlertDialog.getInfoAlertDialog("Entry is successfully saved")
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                
            })
        }
    }
    
    func Pass(data: Any?, sender: Sender) {
        if sender == .DatePicker{
            let date = data as! String
            self.dateLabel.text = date
        }else{
            let text = data as! String
            self.tagTypesBtn.setTitle(text, for: .normal)
            
            self.selectedTagType = data as? String
            var filtered = [TagDto]()
            
            for tag in self.allTags{
                if tag.tagType == self.selectedTagType || self.selectedTagType == "All Types"{
                    filtered.append(tag)
                    tag.isSelected = self.selectedTags.index(of: tag.id) != nil
                }
            }
            
            for view in self.tagViews{
                for sview in view.subviews{
                    sview.removeFromSuperview()
                }
                self.tagsStackView.removeArrangedSubview(view)
                view.removeFromSuperview()
            }
            self.tagsStackView.layoutIfNeeded()
            
            self.tagViews = UIHelper.add(tags: filtered, toStackView: self.tagsStackView)
            for view in self.tagViews{
                let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(NewToDoViewController.tagViewTapped))
                view.isUserInteractionEnabled = true
                view.addGestureRecognizer(tapGesture)
            }
            
       }
    }
    
    @objc func tagViewTapped(sender:UITapGestureRecognizer){
        let tagView = sender.view! as UIView
        
        if tagView.backgroundColor == UIHelper.selectedTagViewColor{
            tagView.backgroundColor = UIColor.white
            let index = self.selectedTags.index(of: tagView.tag)!
            self.selectedTags.remove(at: index)
        }else{
            tagView.backgroundColor = UIHelper.selectedTagViewColor
            self.selectedTags.append(tagView.tag)
        }
        
        tagsStackView.layoutIfNeeded()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let datePickerVC = segue.destination as? DatePickerModalViewController{
            self.datePickerVC = datePickerVC
            self.datePickerVC?.delegate = self
        }
        
        if let pickerVC = segue.destination as? TagTypePickerViewController{
            pickerVC.delegate = self
            pickerVC.tagTypes = self.tagTypes
        }
    }
 

}
