//
//  ResetPasswordViewController.swift
//  ActivityJournaling
//
//  Created by Predrag Filipovic on 10/8/18.
//  Copyright © 2018 Devops-X. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    let authService = AuthenticationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func resetPasswordDidTap(_ sender: Any) {
        AuthenticationService.setToken(token: "test")
        let spinner = UIViewController.displaySpinner(onView: self.view)
        
        self.authService.resetPassword(email: self.email.text){
            UIViewController.removeSpinner(spinner: spinner)
            
            if let errorParam = $0 {
                let dlg = AlertDialog.getErrorAlertDialog(errorParam.description!)
                DispatchQueue.main.async(execute: {
                    self.present(dlg, animated: true, completion: nil)
                })
                return
            }
            
            return
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
